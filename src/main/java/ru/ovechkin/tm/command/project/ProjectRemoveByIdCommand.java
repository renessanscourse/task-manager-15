package ru.ovechkin.tm.command.project;

import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CmdConst.PROJECT_REMOVE_BY_ID;
    }

    @Override
    public String description() {
        return "Remove project by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE PROJECT]");
        System.out.print("ENTER PROJECT ID: ");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().removeProjectById(userId, id);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[COMPLETE]");
    }

}