package ru.ovechkin.tm.command.project;

import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return CmdConst.PROJECT_UPDATE_BY_ID;
    }

    @Override
    public String description() {
        return "Update project by id";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PROJECT]");
        System.out.print("ENTER PROJECT ID: ");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findProjectById(userId, id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NEW PROJECT NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW PROJECT DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Project projectUpdated = serviceLocator.getProjectService().
                updateProjectById(userId, id, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

}