package ru.ovechkin.tm.command.system;

import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.ArgumentConst;
import ru.ovechkin.tm.constant.CmdConst;

import java.util.List;

public final class CommandsAllCommand extends AbstractCommand {

    @Override
    public String arg() {
        return ArgumentConst.ARG_COMMANDS;
    }

    @Override
    public String name() {
        return CmdConst.CMD_COMMANDS;
    }

    @Override
    public String description() {
        return "Show available commands";
    }

    @Override
    public void execute() {
        final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (final AbstractCommand command : commands) System.out.println(command.name());
    }

}