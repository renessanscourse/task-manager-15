package ru.ovechkin.tm.repository;

import ru.ovechkin.tm.api.repository.ICommandRepository;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.command.auth.*;
import ru.ovechkin.tm.command.project.*;
import ru.ovechkin.tm.command.system.*;
import ru.ovechkin.tm.command.task.*;

import java.util.*;

public class CommandRepository implements ICommandRepository {

    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        commandList.add(new HelpCommand());
        commandList.add(new AboutCommand());
        commandList.add(new VersionCommand());
        commandList.add(new CommandsAllCommand());
        commandList.add(new ArgumentsAllCommand());
        commandList.add(new SystemInfoCommand());
        commandList.add(new TaskCreateCommand());
        commandList.add(new TaskClearCommand());
        commandList.add(new TaskListCommand());
        commandList.add(new TaskShowByIdCommand());
        commandList.add(new TaskShowByIndexCommand());
        commandList.add(new TaskShowByNameCommand());
        commandList.add(new TaskUpdateByIdCommand());
        commandList.add(new TaskUpdateByIndexCommand());
        commandList.add(new TaskRemoveByIdCommand());
        commandList.add(new TaskRemoveByIndexCommand());
        commandList.add(new TaskRemoveByNameCommand());
        commandList.add(new ProjectCreateCommand());
        commandList.add(new ProjectClearCommand());
        commandList.add(new ProjectListCommand());
        commandList.add(new ProjectShowByIdCommand());
        commandList.add(new ProjectShowByIndexCommand());
        commandList.add(new ProjectShowByNameCommand());
        commandList.add(new ProjectUpdateByIdCommand());
        commandList.add(new ProjectUpdateByIndexCommand());
        commandList.add(new ProjectRemoveByIdCommand());
        commandList.add(new ProjectRemoveByIndexCommand());
        commandList.add(new ProjectRemoveByNameCommand());
        commandList.add(new ShowProfileCommand());
        commandList.add(new RegistryCommand());
        commandList.add(new LoginCommand());
        commandList.add(new LogoutCommand());
        commandList.add(new UpdatePasswordCommand());
        commandList.add(new UpdateProfileCommand());
        commandList.add(new ExitCommand());
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return commandList;
    }

}