package ru.ovechkin.tm.exeption;

public class WrongCurrentPasswordException extends RuntimeException {

    public WrongCurrentPasswordException() {
        super("Error! wrong current password...");
    }

}