package ru.ovechkin.tm.exeption.user;

public class AlreadyLoggedInException extends RuntimeException {

    public AlreadyLoggedInException() {
        super("Error! You are already logged in. Please logout before logging in");
    }

}