package ru.ovechkin.tm.exeption;

public class NotLoggedInException extends RuntimeException {

    public NotLoggedInException() {
        super("Error! You are not logged in...");
    }

}