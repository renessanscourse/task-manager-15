package ru.ovechkin.tm.exeption;

public class WrongCommandException extends RuntimeException {

    public WrongCommandException(final String command) {
        super("Error! Command `"+ command +"` does not exist...");
    }

}